<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MarkDown</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 bg-light sidebar" style="margin:0">
                <div class="p-3 mb-2 bg-dark text-white text-center" style="padding:0">
                    <h4>MarkdownEditor <a href="" id="new" class="text-success"><i class="fas fa-plus-circle"></i></a></h4>
                </div>

                <div class="sidebar-sticky">
                    <ul class="nav flex-column" id="nav">
                        @forelse($documents As $document)
                            <li class="nav-item">
                                <a class="nav-link" href="#" id="{{$document->id}}">
                                    <i class="fas fa-file-alt"></i>
                                    {{$document->name}}  <span class="time"> {{\Carbon\Carbon::parse($document->updated_at ? $document->updated_at : $codument->created_at)->diffForHumans()}}</span>
                                </a>
                            </li>
                        @empty
                            <li>Sin Documentos</li>
                        @endforelse
                    </ul>
                </div>
            </nav>
          
      
            <div class="col-5" style="padding:0">
                <form>
                    <div class="form-group">
                        <textarea class="form-control" id="textControl" style="height: 100vh;"></textarea>
                    </div>
                </form>
            </div>
            <div class="col-5">
                <div class="text-right">
                    <a class="btn text-success" href="#" id="save"><i class="fas fa-save"></i> Save</a> 
                    <a class="btn text-danger" href="#" id="delete"><i class="fas fa-trash"></i> Delete</a>
                </div>

                <div  class="mt-2">
                    <pre id="preview">
                    </pre>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        $(document).ready(function(){

            var id = false;
            var name = false;

            $('#nav').on('click', '.nav-link', function(ev){
                ev.preventDefault();

                $('.nav-link').removeClass('active');

                $(this).addClass('active');

                id = $(this).attr('id');

                if(id>0)
                {
                    axios.get(`./document/${id}`).then((response) => {

                        $("#textControl").val(response.data.text)
                        $("#preview").html(response.data.text)
                    });
                }
                else
                {
                        $("#textControl").val('')
                        $("#preview").html('')
                }
            });


            $('#textControl').keyup(function(){
                $("#preview").html($(this).val());
            });

            $("#new").click(function(ev){
                ev.preventDefault();

                $('.nav-link').removeClass('active');

                id = 'new';
                name = prompt("Please enter  name", "New Documment");

                if(name != null)
                {
                    $(".nav").prepend(`<li class="nav-item">
                                            <a class="nav-link active" id="new" href="#" >
                                                <i class="fas fa-file-alt"></i>
                                            ${name}  <span class="time"> 2 Seconds ago</span>
                                            </a>
                                        </li>`);
                    
                    $("#textControl").val('');
                    $("#preview").html('');
                }
               
                
            });

            $("#save").click(function(ev){
                ev.preventDefault();

                    const params = {
                        id: id,
                        name: name,
                        text: $('#textControl').val()
                    };  
                    
                    axios.post('./save', params).then((response) => {

    
     
                        if(response.data.id>0)
                        {
                            $(".nav-link[id='new']").attr('id', response.data.id);
                            alert('Save Success');
                        }
                        else
                        {
                            alert('ERRROR');
                        }
                        
                    });
            });

            $("#delete").click(function(ev){
                ev.preventDefault();

                if(confirm('Cofirm Delete'))
                {
                    const params = {
                        id: id
                    };  
                    
                    axios.post('./delete', params).then((response) => {
                        
                        $(".nav-link[id="+id+"]").parent().remove();
                        $("#textControl").val('');
                        $("#preview").html('');

                        alert('Delete Success');
                    });
                }
            });
        });
    </script>
</body>
</html>