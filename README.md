# Markdown By Kontramundo Systems

Markdown Editor

Create / Edit / Destroy documents.

Documents must be saved on the server-side.

Display the HTML rendered version of the markdown in real-time.

## Installation

Use Laravel Framework

The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

PHP >= 7.1.3

BCMath PHP Extension

Ctype PHP Extension

JSON PHP Extension

Mbstring PHP Extension

OpenSSL PHP Extension

PDO PHP Extension

Tokenizer PHP Extension

XML PHP Extension

Example using Valet:

[VALET INSTALL](https://laravel.com/docs/5.8/valet)

```bash
npm install
```

```bash
Dump Database markdown.sql
```

## TEST
[PREVIEW](https://kontramundo.com/dev/markdown/)


## Nota
Sin duda el proyecto se puede mejorar, con validaciones mas estrictas, control de usuarios y muchas funcionalidades extras. Pero al ser un proyecto de prueba decidi hacerlo funcional con lo minimo indispensable.

Quedo a sus ordenes para cualquier duda al respecto.

Gracias


## License
[MIT](https://choosealicense.com/licenses/mit/)