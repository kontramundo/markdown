<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MarkdownController@index');
Route::get('document/{id}', 'MarkdownController@document');
Route::post('save', 'MarkdownController@save');
Route::post('delete', 'MarkdownController@delete');
