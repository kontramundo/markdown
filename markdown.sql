/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : 127.0.0.1:3306
 Source Schema         : markdown

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 22/06/2019 14:18:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of documents
-- ----------------------------
BEGIN;
INSERT INTO `documents` VALUES (1, 'test', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '2019-06-22 12:37:34', '2019-06-22 13:29:39', NULL);
INSERT INTO `documents` VALUES (2, 'New Documment', 'test', '2019-06-22 13:49:33', '2019-06-22 13:57:15', '2019-06-22 13:57:15');
INSERT INTO `documents` VALUES (3, 'New Documment', 'Prueba', '2019-06-22 14:01:04', '2019-06-22 14:15:23', NULL);
INSERT INTO `documents` VALUES (4, 'New Documment', 'dfdfdfdf', '2019-06-22 14:01:36', '2019-06-22 14:12:41', '2019-06-22 14:12:41');
INSERT INTO `documents` VALUES (5, 'New Documment', 'prueba', '2019-06-22 14:04:00', '2019-06-22 14:12:11', '2019-06-22 14:12:11');
INSERT INTO `documents` VALUES (6, 'New Documment', 'fgfgfg', '2019-06-22 14:04:25', '2019-06-22 14:11:28', '2019-06-22 14:11:28');
INSERT INTO `documents` VALUES (7, 'New Documment', NULL, '2019-06-22 14:06:01', '2019-06-22 14:06:47', '2019-06-22 14:06:47');
INSERT INTO `documents` VALUES (8, 'xxx', 'sddsdddds', '2019-06-22 14:06:57', '2019-06-22 14:13:49', '2019-06-22 14:13:49');
INSERT INTO `documents` VALUES (9, 'New Documment', 'fdfdfd', '2019-06-22 14:08:52', '2019-06-22 14:10:57', '2019-06-22 14:10:57');
INSERT INTO `documents` VALUES (10, 'New Documment', 'er', '2019-06-22 14:16:52', '2019-06-22 14:16:52', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
