<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use Carbon\Carbon;
use Response;

class MarkdownController extends Controller
{
    public function index(){

        $documents = Document::orderBy('created_at', 'desc')->get();

        return view('markdown', compact('documents'));
    }

    public function document($id)
    {
        $document = Document::find($id);

        return $document->toJson();
    }

    public function save(Request $request)
    {
        if($request->id>0)
        {
            $document = Document::find($request->id);
                $document->text = $request->text;
            $document->save();
        }
        else
        {
            $document = new Document();
                $document->name = $request->name;
                $document->text = $request->text;
            $document->save();
        }
        
        if($document->id)
        {
            return Response::json(
                [
                    'success' => true,
                    'id' => $document->id, 
                ]
            );
        }
    }

    public function delete(Request $request)
    {
        Document::find($request->id)->delete();

        return response('Success', 200);
    }
}
